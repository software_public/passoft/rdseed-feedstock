#!/usr/bin/env bash -ex

# Rdseed's makefile does an odd thing of "CC=cc"
# so this works around:
ln -s $CC $BUILD_PREFIX/bin/cc

make
mkdir -p $PREFIX/bin
pwd
cp rdseed $PREFIX/bin/rdseed
